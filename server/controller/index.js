const express = require('express');
const hero = require('./routes/hero');
const services = require('./routes/services');
const titles = require('./routes/titles');
const blogs = require('./routes/blogs');
const statistics = require('./routes/statistics');
const testimonials = require('./routes/testimonials');
const team = require('./routes/team');
const options = require('./routes/options');
const partners = require('./routes/partners');
const Auth = require('./auth');
const users = require('./routes/users');
const file = require('./routes/file');
const pricingPlans = require('./routes/pricingPlans');
const features = require('./routes/features');
const whyUs = require('./routes/whyUs');
const core = require('./routes/core');
const portfolio = require('./routes/portfolio');
const about = require('./routes/about');
const install = require('./routes/install');
const contactus = require('./routes/contactus');
const newsletters = require('./routes/newsletters');
const { checkRoute } = require('./checkRoute');

const router = express.Router();

router
  .use('/install', install)
  .use('/files', file)
  .use('/titles', titles)
  .use('/statistics', statistics)
  .use('/options', options)
  .use('/users', users)
  .use('/pricingPlans', checkRoute, pricingPlans)
  .use('/features', checkRoute, features)
  .use('/whyUs', checkRoute, whyUs)
  .use('/about', checkRoute, about)
  .use('/core', checkRoute, core)
  .use('/testimonials', checkRoute, testimonials)
  .use('/hero', checkRoute, hero)
  .use('/services', checkRoute, services)
  .use('/blogs', checkRoute, blogs)
  .use('/team', checkRoute, team)
  .use('/partners', checkRoute, partners)
  .use('/portfolio', checkRoute, portfolio)
  .use('/contactus', checkRoute, contactus)
  .use('/newsletters', checkRoute, newsletters);

module.exports = router;
