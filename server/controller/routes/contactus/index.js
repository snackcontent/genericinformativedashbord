const express = require('express');
const contactus = require('./contactus');

const router = express.Router();
router
  .get('/', contactus.get);

module.exports = router;
