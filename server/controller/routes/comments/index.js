const express = require('express');
const comments = require('./comments');

const router = express.Router();
router
  .get('/', comments.get)
  .post('/', comments.post)
  .delete('/', comments.delete)
  .post('/update', comments.update);

module.exports = router;
