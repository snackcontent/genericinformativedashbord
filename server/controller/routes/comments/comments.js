const {
  comments,
  blogs,
} = require('../../../database/modals');


exports.get = async (request, response) => {
  try {
    const result = await comments.findAll({
      order: [['id', 'DESC']],
      include: [{ model: blogs }],
    });
    return response.status(200).send(result);
  } catch (error) {
    response.status(500).send('Server Error');
  }
};

exports.post = async (request, response) => {
  try {
    const {
      seo, body, name, email,
    } = request.body;
    const postId = await blogs.findOne({ where: { seo }, raw: true, attributes: ['id'] });
    if (postId) {
      const { id } = postId;
      const newComment = {
        body, name, email, post_id: id,
      };
      await comments.create(newComment);
      response.status(200).send({ message: 'Your comment has been added, just wait for the approve' });
    } else {
      response.status(400).send('Blog not exist');
    }
  } catch (error) {
    response.status(500).send('Server Error');
  }
};
exports.delete = async (req, res) => {
  try {
    const {
      id,
    } = req.body;

    await comments.destroy({ where: { id } });
    res.status(200).send({ message: 'Comment Deleted' });
  } catch (error) {
    res.status(500).send('Internal server error');
  }
};

exports.update = async (req, res) => {
  try {
    const {
      data: { id },
    } = req.body;
    const commentData = await comments.findByPk(id, {
      attributes: ['approve'],
      raw: true,
    });
    const { approve } = commentData;
    if (approve === '0') {
      await comments.update({ approve: '1' }, { where: { id } });
      res.status(200).send({ message: 'Comment Updated' });
    } else if (approve === '1') {
      await comments.update({ approve: '0' }, { where: { id } });
      res.status(200).send({ message: 'Comment Updated' });
    } else {
      res.status(500).send('Internal server error');
    }
  } catch (error) {
    res.status(500).send('Internal server error');
  }
};
