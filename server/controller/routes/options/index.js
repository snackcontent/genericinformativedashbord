const express = require('express');
const options = require('./options');

const router = express.Router();
router
  .get('/', options.get)
  .post('/', options.update);

module.exports = router;
