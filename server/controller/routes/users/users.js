const bcryptjs = require('bcryptjs');
const validator = require('validator');
const { users } = require('../../../database/modals');

exports.getAll = async (request, response) => {
  try {
    const result = await users.findAll({ order: [['id', 'DESC']] });
    response.status(200).send(result);
  } catch (error) {
    response.status(500).send('Server Error');
  }
};

exports.post = async (request, response) => {
  try {
    const { body } = request;
    const {
      email, password, name, pic, rule,
    } = body;

    if (
      email
      && email.trim()
      && password
      && name
      && pic
      && rule
      && password.trim()
      && name.trim()
      && pic.trim()
    ) {
      const result = await users.count({ where: { email } });
      if (result === 0) {
        bcryptjs.hash(password, 10, async (err, hash) => {
          if (err) {
            response.status(500).send('Internal Server Error');
          }
          const newUser = {
            name,
            email,
            password: hash,
            rule,
            pic,
          };
          await users.create(newUser);
          response
            .status(200)
            .send({ message: 'User Added' });
        });
      } else {
        response.status(400).send({ message: 'Email is already exist !' });
      }
    } else {
      response.status(400).send({
        message: 'Invalid inputs, please note the type of each input',
      });
    }
  } catch (error) {
    response.status(500).send({ message: 'Internal Server Error' });
  }
};

exports.delete = (req, res) => {
  try {
    const { id } = req.body;
    users.destroy({ where: { id } });
    res.status(200).send({ message: 'User Deleted' });
  } catch (error) {
    res.status(500).send({ message: 'Internal Server Error' });
  }
};

exports.getUserById = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await users.findById(id);
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send({ message: 'Internal Server Error' });
  }
};

exports.updateUser = async (req, res) => {
  try {
    const {
      data,
      params: { id },
    } = req.body;

    const {
      email, name, rule,
    } = data;

    if (
      email.trim()
      && name.trim()
      && validator.isLength(name, { min: 1, max: 20 })
    ) {
      const checkEmail = await users.findOne({ where: { email }, raw: true });
      if (checkEmail && checkEmail.id !== parseInt(id, 10)) {
        res
          .status(400)
          .send({ message: 'Sorry !, this email is already exist' });
      } else if (!data.password) {
        users.update(data, {
          where: { id },
        });
        res.status(200).send({ message: 'User Updated' });
      } else if (data.password.trim()) {
        bcryptjs.hash(data.password, 10, (err, hashedPass) => {
          if (err) {
            res.status(500).send({ message: 'Internal server Error' });
          }
          data.password = hashedPass;
          users.update(data, {
            where: { id },
          });
          res.status(200).send({ message: 'User Updated' });
        });
      } else {
        res.status(400).send({
          message: 'Invalid inputs, enter a valid Password',
        });
      }
    } else {
      res.status(400).send({
        message: 'Invalid inputs, please note the type of each input',
      });
    }
  } catch (error) {
    res.status(500).send({ message: 'Internal Server Error' });
  }
};


exports.getDetailsUser = async (req, res) => {
  try {
    const { id } = req;
    const result = await users.findAll({ attributes: ['name', 'pic', 'id'], where: { id } });
    res.status(200).send(result);
  } catch (error) {
    res.status(500).send({ message: 'Internal Server Error' });
  }
};
